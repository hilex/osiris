package ocmd

import (
	"osiris/cli"
	"osiris/errors"
	"osiris/logger"
	"sync"
)

// AutoTxHandler 自动随机交易，测试用
type AutoTxHandler struct{}

func (handler *AutoTxHandler) Handle(inputs []string, wg *sync.WaitGroup) error {
	defer func() {
		if wg != nil {
			wg.Done()
		}
	}()

	if len(inputs) != 2 {
		return &errors.CommandInputError{
			Msg: "invalid arguments!",
		}
	}

	switch inputs[1] {

	case START:
		cli.StartAutoTx()
		logger.Println("auto tx started.")

	case STOP:
		cli.StopAutoTx()
		logger.Println("auto tx stop.")
		
	default:
		return &errors.CommandInputError{
			Msg: "invalid arguments!",
		}
	}

	return nil
}

func (hadnler *AutoTxHandler) PrintHelp() {
	logger.Println("usage: autotx start")
	logger.Println("usage: autotx stop")
}
