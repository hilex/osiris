package rand

import (
	"crypto/rand"
	"math/big"
	"osiris/logger"
)

// GenerateNonce 生成大随机数（big.Int.Int64(),[0,max）），出错时返回0
//
//	@param maxNonce 随机数的最大值
//	@return int64
//	@return error
func GenerateNonce(maxNonce *big.Int) (int64, error) {
	randomNonce, err := rand.Int(rand.Reader, maxNonce)
	if err != nil {
		logger.Error(map[string]interface{}{"[ocrypto] [Generate Nonce]": err})
		return big.NewInt(0).Int64(), err
	}

	return randomNonce.Int64(), nil
}
