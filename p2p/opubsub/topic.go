package opubsub

/* import (
	"context"
	"fmt"
	pubsub "github.com/libp2p/go-libp2p-pubsub"
	"osiris/logger"
)

type TopicManager interface {
	topicID() TopicID
	Init(ctx context.Context, ps *pubsub.PubSub) error
	Publish(json []byte) error
	Subcribe()
	handleMsg(json []byte)
}

type BaseTopicManager struct {
	ctx   context.Context
	sub   *pubsub.Subscription
	topic *pubsub.Topic
}

func (mng BaseTopicManager) Init(ctx context.Context, ps *pubsub.PubSub) error {
	mng.ctx = ctx
	// join the pubsub topic
	topic, err := ps.Join(string(mng.topicID()))
	if err != nil {
		logger.Error(map[string]interface{}{fmt.Sprintf("[p2p] [TopicManager.Init] join pubsub with topic %s", mng.topicID()): err})
		return err
	}

	// and subscribe to it
	sub, err := topic.Subscribe()
	if err != nil {
		logger.Error(map[string]interface{}{fmt.Sprintf("[p2p] [TopicManager.Init] subscribe topic %s", mng.topicID()): err})
		return err
	}
	mng.sub = sub

	return nil
}

func (mng BaseTopicManager) Publish(json []byte) error {
	return mng.topic.Publish(mng.ctx, json)
}

func (mng BaseTopicManager) Subcribe() {
	go func() {
		for {
			msg, err := mng.sub.Next(mng.ctx)
			if err != nil {
				logger.Error(map[string]interface{}{"[p2p] [TopicManager.Subscribe] sub.Next()": err})
				return
			}

			mng.handleMsg(msg.Data)
		}
	}()
}

func (mng BaseTopicManager) topicID() TopicID {
	//implement by sub class
	return ""
}

func (mng BaseTopicManager) handleMsg(json []byte) {
	//implement by sub class
}
 */