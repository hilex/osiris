package opubsub

import (
	/* "context"
	"fmt"
	"osiris/logger"

	pubsub "github.com/libp2p/go-libp2p-pubsub" */
)

type TopicID string

const (
	TxTopicID TopicID = "/pubsub/topic"
)

type PubSubManager struct {
	//topicManagers map[TopicID]TopicManager
}

/* /* func (mng *PubSubManager) AddTopicManager(topicManager TopicManager) *PubSubManager {
	mng.topicManagers[topicManager.topicID()] = topicManager
	return mng
}

func (mng *PubSubManager) InitPubSub(ctx context.Context, ps *pubsub.PubSub) {
	for _, mng := range mng.topicManagers {
		mng.Init(ctx, ps)
		mng.Subcribe()
	}
}

func (mng *PubSubManager) Publish(topicID TopicID, json []byte) {
	if topicManager, exist := mng.topicManagers[topicID]; exist {
		topicManager.Publish(json)
	} else {
		logger.Error(map[string]interface{}{fmt.Sprintf("[pubsub] [PubSubManager.Publish] topic: %s", topicID): "topic does not exist!"})
	}
} */
 