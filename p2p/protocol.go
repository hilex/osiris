package p2p

import (
	"sync"

	"github.com/libp2p/go-libp2p/core/network"
	peer "github.com/libp2p/go-libp2p/core/peer"
	"github.com/libp2p/go-libp2p/core/protocol"
)

// Protocol 对等节点通信协议
type Protocol interface {

	// protocolName 协议名称
	//  @return protocol.ID
	protocolName() protocol.ID

	// openStream 开启协议对应的网络流
	//  @param peerID  目标节点
	//  @param jsonStr 传输的json字符串
	//  @param wg 用于通知通信结束的wg
	openStream(peerID peer.ID, jsonStr string, wg *sync.WaitGroup)

	// handleStream 处理协议对应的网络流
	//  @param s
	handleStream(s network.Stream)
}
