// package 自定义日志
package logger

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"path"
	"runtime/debug"
	"sync"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

// var
//
//	@param AllowStdLog 是否允许在终端上打印日志
//	@param logPath 日志文件路径
var (
	allowStdLog bool
	configMutex *sync.RWMutex
	logPath     string
)

func init() {
	allowStdLog = true
	logPath = "./runtime/log"
	configMutex = new(sync.RWMutex)
	// 设置日志格式为json格式
	logrus.SetFormatter(&logrus.JSONFormatter{
		TimestampFormat: "2006-01-02 15:04:05",
	})
	logrus.SetReportCaller(false)
}

func EnableStdLog() {
	if IsAllowStdLog() {
		return
	}

	configMutex.Lock()
	allowStdLog = true
	configMutex.Unlock()
}

func DisableStdLog() {
	if !IsAllowStdLog() {
		return
	}

	configMutex.Lock()
	allowStdLog = false
	configMutex.Unlock()
}

func IsAllowStdLog() bool {
	configMutex.RLock()
	allow := allowStdLog
	configMutex.RUnlock()
	return allow
}

func Printf(format string, args ...any) {
	if !allowStdLog {
		return
	}
	fmt.Printf(format, args...)
}

func Println(args ...any) {
	if !allowStdLog {
		return
	}
	fmt.Println(args...)
}

func Write(msg string, filename string) {
	setOutPutFile(logrus.InfoLevel, filename)
	logrus.Info(msg)
}

func Debug(fields logrus.Fields, args ...interface{}) {
	setOutPutFile(logrus.DebugLevel, "debug")
	logrus.WithFields(fields).Debug(args...)
}

func Info(fields logrus.Fields, args ...interface{}) {
	setOutPutFile(logrus.InfoLevel, "info")
	logrus.WithFields(fields).Info(args...)
}

func Warn(fields logrus.Fields, args ...interface{}) {
	setOutPutFile(logrus.WarnLevel, "warn")
	logrus.WithFields(fields).Warn(args...)
}

func Fatal(fields logrus.Fields, args ...interface{}) {
	setOutPutFile(logrus.FatalLevel, "fatal")
	logrus.WithFields(fields).Fatal(args...)
}

func Error(fields logrus.Fields, args ...interface{}) {
	setOutPutFile(logrus.ErrorLevel, "error")
	logrus.WithFields(fields).Error(args...)
}

func Panic(fields logrus.Fields, args ...interface{}) {
	setOutPutFile(logrus.PanicLevel, "panic")
	logrus.WithFields(fields).Panic(args...)
}

func Trace(fields logrus.Fields, args ...interface{}) {
	setOutPutFile(logrus.TraceLevel, "trace")
	logrus.WithFields(fields).Trace(args...)
}

func SetSubPath(subPath string) {
	if subPath != "" {
		logPath = logPath + "/" + subPath
	}
}

func setOutPutFile(level logrus.Level, logName string) {
	if _, err := os.Stat(logPath); os.IsNotExist(err) {
		err = os.MkdirAll(logPath, 0777)
		if err != nil {
			panic(fmt.Errorf("create log dir '%s' error: %s", logPath, err))
		}
	}

	timeStr := time.Now().Format("2006-01-02")
	fileName := path.Join(logPath, logName+"_"+timeStr+".log")

	var err error
	os.Stderr, err = os.OpenFile(fileName, os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0644)
	if err != nil {
		fmt.Println("open log file err", err)
	}
	logrus.SetOutput(os.Stderr)
	logrus.SetLevel(level)
}

func LoggerToFile() gin.LoggerConfig {

	if _, err := os.Stat(logPath); os.IsNotExist(err) {
		err = os.MkdirAll(logPath, 0777)
		if err != nil {
			panic(fmt.Errorf("create log dir '%s' error: %s", logPath, err))
		}
	}

	timeStr := time.Now().Format("2006-01-02")
	fileName := path.Join(logPath, "success_"+timeStr+".log")

	os.Stderr, _ = os.OpenFile(fileName, os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0644)

	var conf = gin.LoggerConfig{
		Formatter: func(param gin.LogFormatterParams) string {
			return fmt.Sprintf("%s - %s \"%s %s %s %d %s \"%s\" %s\"\n",
				param.TimeStamp.Format("2006-01-02 15:04:05"),
				param.ClientIP,
				param.Method,
				param.Path,
				param.Request.Proto,
				param.StatusCode,
				param.Latency,
				param.Request.UserAgent(),
				param.ErrorMessage,
			)
		},
		Output: io.MultiWriter(os.Stdout, os.Stderr),
	}

	return conf
}

func Recover(c *gin.Context) {
	defer func() {
		if err := recover(); err != nil {
			if _, errDir := os.Stat(logPath); os.IsNotExist(errDir) {
				errDir = os.MkdirAll(logPath, 0777)
				if errDir != nil {
					panic(fmt.Errorf("create log dir '%s' error: %s", logPath, errDir))
				}
			}

			timeStr := time.Now().Format("2006-01-02")
			fileName := path.Join(logPath, "error_"+timeStr+".log")

			f, errFile := os.OpenFile(fileName, os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0644)
			if errFile != nil {
				fmt.Println(errFile)
			}
			timeFileStr := time.Now().Format("2006-01-02 15:04:05")
			f.WriteString("panic error time:" + timeFileStr + "\n")
			f.WriteString(fmt.Sprintf("%v", err) + "\n")
			f.WriteString("stacktrace from panic:" + string(debug.Stack()) + "\n")
			f.Close()
			c.JSON(http.StatusOK, gin.H{
				"code": 500,
				"msg":  fmt.Sprintf("%v", err),
			})
			//终止后续接口调用，不加的话recover到异常后，还会继续执行接口里后续代码
			c.Abort()
		}
	}()
	c.Next()
}
