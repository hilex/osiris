package errors

type CommandInputError struct {
	Msg string
}

func (err *CommandInputError) Error() string {
	return "command error: " + err.Msg
}
