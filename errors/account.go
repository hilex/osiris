// package 自定义错误
package errors

type PrivKeyNotFoundError struct {
	Msg string
}

func (err *PrivKeyNotFoundError) Error() string {
	return "privKey not founded: " + err.Msg
}

type AccountExistError struct{}

func (err *AccountExistError) Error() string {
	return "account already exist"
}

type AccountNotExistError struct{}

func (err *AccountNotExistError) Error() string {
	return "account does not exist"
}
