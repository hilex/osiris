package errors

type UnknownTxTypeError struct{}

func (err *UnknownTxTypeError) Error() string {
	return "unkown tx type "
}

type UnknownStateTrieError struct{}

func (err *UnknownStateTrieError) Error() string {
	return "unkown state trie "
}

type UnknownAccountTrieError struct{}

func (err *UnknownAccountTrieError) Error() string {
	return "unkown account state trie "
}

type UnknownPeerTrieError struct{}

func (err *UnknownPeerTrieError) Error() string {
	return "unknown peer state trie"
}

type PeerExistError struct{}

func (err *PeerExistError) Error() string {
	return "peer already exist in the trie"
}

type TxCommitError struct{}

func (err *TxCommitError) Error() string {
	return "fail to commit tx"
}

type TxUndoError struct{}

func (err *TxUndoError) Error() string {
	return "fail to undo tx"
}
