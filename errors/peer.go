package errors

type PeerVerifyError struct {
	Msg string
}

func (err *PeerVerifyError) Error() string {
	return "peer verify fail: " + err.Msg
}

type PeerNotExistError struct{}

func (err *PeerNotExistError) Error() string {
	return "peer does not exist"
}

type NoAvailablePeerError struct {
	Msg string
}

func (err *NoAvailablePeerError) Error() string {
	return "no available peer: " + err.Msg
}
