package errors

type BlockVerifyError struct {
	Msg string
}

func (err *BlockVerifyError) Error() string {
	return "block verify error: " + err.Msg
}
