package routine

// AsyncRoutine 异步协程，可以通过指定通道等待实现异步调用
type AsyncRoutine struct {

	// Operation 协程的执行方法
	Operation func(args ...interface{}) []interface{}

	// Args 方法参数
	Args []interface{}

	// 订阅方法执行完毕的通道
	SubscribeChans []chan interface{}
}

func (routine *AsyncRoutine) Execute() {
	go func() {
		results := routine.Operation(routine.Args...)
		for _, ch := range routine.SubscribeChans {
			ch <- results
		}
	}()
}
