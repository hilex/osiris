package state

import (
	//"fmt"
	"osiris/config"
	"osiris/dto"
	"osiris/errors"
	"osiris/logger"
	"osiris/ocrypto/ecdsa"
	"osiris/core/routine"
)

// TransformTxHandler 代币转移交易
type TransformTxHandler struct {
	TxData dto.TxData
}

func (txHandler TransformTxHandler) Verify(tries ...interface{}) bool {
	trie := tries[0]
	tempAccountTrie, ok := trie.(*AccountStateTrie)
	if !ok {
		logger.Warn(map[string]interface{}{"[state] [Verify Transform TX]": "unknown trie!"})
		return false
	}

	//检查来源账户地址是否存在
	fromLeaf, fromExist := tempAccountTrie.GetLeaf(ecdsa.StrToAccountBytes(txHandler.TxData.FromAddr))
	if !fromExist {
		logger.Warn(map[string]interface{}{"[state] [Verify Transform TX]": "source account addr does not exist!"})
		return false
	}

	//检查来源账户的余额是否足够
	if fromLeaf.Asset < txHandler.TxData.Amount {
		logger.Warn(map[string]interface{}{"[state] [Verify Transform TX]": "source account addr does not have enough asset!"})
		return false
	}

	//检查目的账户地址是否存在
	_, toExist := tempAccountTrie.getNonce(txHandler.TxData.ToAddr)
	if !toExist {
		logger.Warn(map[string]interface{}{"[state] [Verify Transform TX]": "destination account addr does not exist!"})
		return false
	}

	//检查来源账户nonce是否合法
	if fromLeaf.Nonce == txHandler.TxData.Nonce {
		return true
	}

	logger.Warn(map[string]interface{}{"[state] [Verify Transform TX]": "nonce does not match!"})
	return false
}

func (txHandler TransformTxHandler) Commit(tries ...interface{}) bool {
	trie := tries[0]
	tempAccountTrie, ok := trie.(*AccountStateTrie)
	if !ok {
		err := &errors.UnknownStateTrieError{}
		logger.Error(map[string]interface{}{"[state] [Verify Transform TX]": err})
		return false
	}

	//修改来源账户状态：扣金额、nonce+1
	fromAccountAddrBytes := ecdsa.StrToAccountBytes(txHandler.TxData.FromAddr)
	fromLeaf, fromExist := tempAccountTrie.GetLeaf(fromAccountAddrBytes)
	if !fromExist {
		logger.Error(map[string]interface{}{"[state] [Commit Transform TX]": "source account addr does not exist!"})
		return false
	}
	fromLeaf.CommitAssetChange(-txHandler.TxData.Amount, txHandler.TxData.CoinSince)
	//logger.Debug(map[string]interface{}{"[state] [Commit Transform TX]": fmt.Sprintf("hash: %s,  fromAddr: %s,  amount: %d, current: %d", txHandler.TxData.TxHash, txHandler.TxData.FromAddr, txHandler.TxData.Amount, fromLeaf.Asset)})

	//如果nonce达到了配置里的最大nonce值，从0开始（为了区别账户创建时的最小随机数1），否则+1
	if fromLeaf.Nonce == config.MaxAccountNonce.Int64() {
		fromLeaf.Nonce = 0
	} else {
		fromLeaf.Nonce += 1
	}

	//修改目的账户状态：加金额
	toAccountAddrBytes := ecdsa.StrToAccountBytes(txHandler.TxData.ToAddr)
	toLeaf, toExist := tempAccountTrie.GetLeaf(toAccountAddrBytes)
	if !toExist {
		logger.Error(map[string]interface{}{"[state] [Commit Transform TX]": "destination account addr does not exist!"})
		return false
	}
	toLeaf.CommitAssetChange(txHandler.TxData.Amount, txHandler.TxData.CoinSince)
	//logger.Debug(map[string]interface{}{"[state] [Commit Transform TX]": fmt.Sprintf("hash: %s,  toAddr: %s,  amount: %d, current: %d", txHandler.TxData.TxHash, txHandler.TxData.ToAddr, txHandler.TxData.Amount, toLeaf.Asset)})

	batch := routine.AtomicRoutineBatch{}
	return batch.Add(&AccountCommitRoutine{
		Kvs: []StateKeyValue{{
			Key: fromAccountAddrBytes,
			Leaf: fromLeaf,
		},{
			Key: toAccountAddrBytes,
			Leaf: toLeaf,
		}},
		Trie: tempAccountTrie,
	}).Execute()
}

func (txHandler TransformTxHandler) Undo(tries ...interface{}) bool {
	trie := tries[0]
	tempAccountTrie, ok := trie.(*AccountStateTrie)
	if !ok {
		err := &errors.UnknownStateTrieError{}
		logger.Error(map[string]interface{}{"[state] [Undo Transform TX]": err})
		return false
	}

	//修改来源账户状态：加金额、nonce-1
	fromAccountAddrBytes := ecdsa.StrToAccountBytes(txHandler.TxData.FromAddr)
	fromLeaf, fromExist := tempAccountTrie.GetLeaf(fromAccountAddrBytes)
	if !fromExist {
		logger.Error(map[string]interface{}{"[state] [Undo Transform TX]": "source account addr does not exist!"})
		return false
	}
	fromLeaf.UndoAssetChange(-txHandler.TxData.Amount, txHandler.TxData.CoinSince)
	if fromLeaf.Nonce == 0 {
		fromLeaf.Nonce = config.MaxAccountNonce.Int64()
	} else {
		fromLeaf.Nonce -= 1
	}

	//修改目的账户状态：减金额
	toAccountAddrBytes := ecdsa.StrToAccountBytes(txHandler.TxData.ToAddr)
	toLeaf, toExist := tempAccountTrie.GetLeaf(toAccountAddrBytes)
	if !toExist {
		logger.Error(map[string]interface{}{"[state] [Undo Transform TX]": "destination account addr does not exist!"})
		return false
	}
	toLeaf.UndoAssetChange(txHandler.TxData.Amount, txHandler.TxData.CoinSince)

	key1 := ecdsa.StrToAccountBytes(txHandler.TxData.FromAddr)
	key2 := ecdsa.StrToAccountBytes(txHandler.TxData.ToAddr)
	
	batch := routine.AtomicRoutineBatch{}
	return batch.Add(&AccountCommitRoutine{
		Kvs: []StateKeyValue{{
			Key: key1,
			Leaf: fromLeaf,
		},{
			Key: key2,
			Leaf: toLeaf,
		}},
		Trie: tempAccountTrie,
	}).Execute()
}
