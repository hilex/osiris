package core

import (
	"os"
	"os/signal"
	"syscall"
)

var (
	Quit chan os.Signal
)

func init() {
	// 创建监听中断信号的通道
	Quit = make(chan os.Signal, 1)
	//说明哪些信号量需要被传递到channel，如果没列出会将所有信号传递到channel；signal包不会因为向channel传递信号而阻塞，如果发现channel阻塞了会直接放弃
	//SIGINT: 2, control+C
	//SIGTERM: 15, 结束程序
	//SIGQUIT：3，control+/
	signal.Notify(Quit, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)
}
