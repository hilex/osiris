package conversion

import (
	"osiris/dto"
	"osiris/model"
)

func TxData2Model(header dto.BlockHeader, txData dto.TxData) model.TxModel {
	txDataJson := toDataMapJson(txData.TxDataMap)
	return model.TxModel{
		ChainID:    header.ChainID,
		Height:     header.Height,
		HeaderHash: header.HeaderHash,
		TxHash:     txData.TxHash,
		TxType:     int(txData.TxType),
		FromAddr:   txData.FromAddr,
		TxDataJson: txDataJson,
	}
}
