// package 数据存储模型
package model

// AccountModel 账户存储模型
type AccountModel struct {

	// ID 自增ID
	ID uint `gorm:"primary_key;auto_increment" json:"ID"`

	// Addr 账户地址
	Addr string `gorm:"column:addr" json:"addr"`

	// Asset 账户余额
	Asset int64 `gorm:"column:asset" json:"asset"`

	// CoinSince 账户资产的平均货币出厂时间戳（用于计算平均币龄=time.Now().Unix()-CoinSince）
	CoinSince int64 `gorm:"column:coinSince" json:"coinSince"`

	// Nonce 当前随机数
	Nonce int64 `gorm:"column:nonce" json:"nonce"`

	// MaxNonce 最大随机数
	MaxNonce int64 `gorm:"column:maxNonce" json:"maxNonce"`
}

// TableName 返回的表明要和数据库中定义的一致
//  @receiver AccountModel
//  @return string
func (AccountModel) TableName() string {
	return "accounts"
}
