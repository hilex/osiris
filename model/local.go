package model

// LocalModel 本地账户地址（在这个节点注册的）
type LocalModel struct {

	// ID 自增ID
	ID uint `gorm:"primary_key;auto_increment" json:"ID"`

	// Addr 账户地址
	Addr string `gorm:"column:addr" json:"addr"`

	// PrivKey 账户的ecdsa私钥，测试的时候才会存储
	PrivKey string `gorm:"column:privkey" json:"privkey"`
}

// TableName 返回的表明要和数据库中定义的一致
//  @receiver LocalAccountModel
//  @return string
func (LocalModel) TableName() string {
	return "locals"
}

type LocalQueuedModel struct {

	// Timestamp 交易创建时的时间戳
	Timestamp int64 `gorm:"column:timestamp" json:"timestamp"`

	// ChainID 属于哪条链
	ChainID int `gorm:"column:chainID" json:"chainID"`

	// TxType 交易类型
	TxType int `gorm:"column:txType" json:"txType"`

	// FromAddr 用于输入的账户地址
	FromAddr string `gorm:"column:fromAddr" json:"fromAddr"`

	// ToAddr 用于输出的账户地址
	ToAddr string `gorm:"column:toAddr" json:"toAddr"`

	// Amount 转移的代币数量
	Amount int64 `gorm:"column:amount" json:"amount"`

	// CoinSince 币龄（*time.Now().Unix()-CoinSince)
	CoinSince int64 `gorm:"column:coinSince" json:"coinSince"`

	// Gas 消耗的燃气量
	Gas int64 `gorm:"column:gas" json:"gas"`

	// GasPrice 愿意提供的燃气价格
	GasPrice int64 `gorm:"column:gsaPrice" json:"gasPrice"`

	// Nonce 随机数
	Nonce int64 `gorm:"column:nonce" json:"nonce"`

	// MaxNonce 最大随机数，达到这个值后从0继续(只有账户注册交易时这个值才有效)
	MaxNonce int64 `gorm:"column:maxNonce" json:"maxNonce"`

	// TxHash 交易数据的Keccak256 Hash
	TxHash string `gorm:"column:txHash" json:"txHash"`

	// AccountSignature 输入账户的签名（ECDSA（Keccak256 Hash））
	AccountSignature string `gorm:"column:accountSignature" json:"accountSignature"`

	// TxDataMap
	TxDataMapJson string `gorm:"column:txDatamapJson" json:"txDatamapJson"`
}

func (LocalQueuedModel) TableName() string {
	return "localqueued"
}
