package model

// BlockHeaderModel 区块头存储模型（理论上区块中的index、hash、height应在区块链中是唯一的）
type BlockHeaderModel struct {

	// Timestamp 区块时间戳
	Timestamp        int64  `gorm:"column:timestamp" json:"timestamp"`    

	// ChainID 属于哪条链
	ChainID          int    `gorm:"column:chainID" json:"chainID"`        

	// Height 区块高度
	Height           int64  `gorm:"column:height" json:"height"`              

	// PreBlockHash 上一个区块哈希值
	PreBlockHash     string `gorm:"column:preBlockHash" json:"preBlockHash"`      

	// HeaderHash 当前区块头哈希
	HeaderHash       string `gorm:"column:headerHash" json:"headerHash"`       

	// AccountStateHash 账户状态树根hash
	AccountStateHash string `gorm:"column:accountStateHash" json:"accountStateHash"` 

	// PeerStateHash 节点状态树根hash
	PeerStateHash    string `gorm:"column:peerStateHash" json:"peerStateHash"`  
	     
	// TxHash 交易树根hash
	TxHash           string `gorm:"column:txHash" json:"txHash"` 

	// ReceiptHash 收据树根hash
	ReceiptHash      string `gorm:"column:receiptHash" json:"receiptHash"`  

	// GasUsed gas使用量
	GasUsed          int    `gorm:"column:gsaUsed" json:"gasUsed"` 

	// GasLimit gas限制
	GasLimit         int    `gorm:"column:gsaLimit" json:"gasLimit"`    

	// ExtraDataJson 扩展数据的json
	ExtraDataJson    string `gorm:"column:extraDataJson" json:"extraDataJson"`       
}

// TableName 返回的表名要和数据库中定义的一致
//  @receiver BlockHeaderModel
//  @return string
func (BlockHeaderModel) TableName() string {
	return "headers"
}
