package model

// 在/p2p/ping协议中，若有节点通过合法性验证，在数据库和host.PeerStore中存储该节点的公钥
// 启动节点时，将数据库中所有的节点的公钥转化为crypto.PubKey，并存入host.PeerStore
// PubKeyModel 存储节点的ed25519公钥,对应host.PeerStore.PubKey()
type PubKeyModel struct {

	// ID 自增ID
	ID uint `gorm:"primary_key;auto_increment" json:"ID"`

	// PeerIDStr peer.ID.String()
	PeerIDStr string `gorm:"column:peerID" json:"peerID"`

	// PubKeyStr ed25519公钥
	PubKeyStr string `gorm:"column:pubkey" json:"pubKey"`
}

// TableName 返回的表名要和数据库中定义的一致
//  @receiver PubKeyModel 
//  @return string 
func (PubKeyModel) TableName() string {
	return "pubkeys"
}
