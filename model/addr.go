package model

// 在/p2p/ping协议中，若有节点通过合法性验证，在数据库和host.PeerStore中存储该节点的MultiAddrString
// 启动节点时，将数据库中所有的MultiAddrString转化为peer.ID和MultiAddr，并存入host.PeerStore
// 主动连接其它节点时，需提供该节点的peer.ID，并从host.PeerStore中找到对应的MultiAddr进行连接

// AddrModel 存储已发现的地址（NAT外）,对应host.PeerStore.Addrs()
type AddrModel struct {

	// ID 自增ID
	ID uint `gorm:"primary_key;auto_increment" json:"ID"`

	// PeerIDStr peer.ID.String()
	PeerIDStr string `gorm:"column:peerID" json:"peerID"`

	// MultiaddrStr ma.Multiaddr.String()
	MultiaddrStr string `gorm:"column:multiaddr" json:"multiaddr"`
}

// TableName 返回的表明要和数据库中定义的一致
//  @receiver PeerModel
//  @return string
func (AddrModel) TableName() string {
	return "addrs"
}
