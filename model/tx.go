package model

// TxModel 已上链的交易
type TxModel struct {

	// ID 自增ID
	ID uint `gorm:"primary_key;auto_increment" json:"ID"`

	// ChainID 属于哪条链
	ChainID int `gorm:"column:chainID" json:"chainID"`

	// Height 存储所在区块的高度
	Height int64 `gorm:"column:height" json:"height"`

	// HeaderHash 存储所在区块的区块头哈希
	HeaderHash string `gorm:"column:headerHash" json:"headerHash"`

	// TxHash 交易hash
	TxHash string `gorm:"column:txHash" json:"txHash"`

	// TxType 交易类型
	TxType int `gorm:"column:txType" json:"txType"`

	// FromAddr 来源地址（可能是账户地址，也可能是peer.ID().String())
	FromAddr string `gorm:"column:fromAddr" json:"fromAddr"`

	// TxDataJson 交易数据的Json
	TxDataJson string `gorm:"column:txDataJson" json:"txDataJson"`
}

func (TxModel) TableName() string {
	return "txs"
}
