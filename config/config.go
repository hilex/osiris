// package 配置信息
package config

import "math/big"

// const 配置信息
const (

	// BaseMySQLConfig MYSQL配置，只需覆盖到数据库地址，在项目启动时会自动根据提供的私钥生成对应的CodeName，并新建（若之前没创建的话）一个名为CodeName的数据库专供持有该私钥的节点使用
	BaseMySQLConfig = "root:123456@tcp(127.0.0.1:3306)/"

	// RedisAddress Redis地址
	RedisAddress = "localhost:6379"

	// RemoteQueuedSize 非本地交易池queued的限制大小，交易数量达到此值后会定期执行queued的清理工作
	RemoteQueuedSize = 400

	// RemotePendingSize 非本地交易池pending的限制大小，交易数量达到此值后会定期执行pending的清理工作
	RemotePendingSize = 800

	// LocalQueuedSize 本地交易池queued的限制大小，交易数量达到此值后会定期执行queued的清理工作
	LocalQueuedSize = 200

	// LocalPendingSize 本地交易池pending的限制大小，交易数量达到此值后会定期执行pending的清理工作
	LocalPendingSize = 400

	// MaxLocalElements 本地交易池中BloomFilter的最大元素数量
	MaxLocalElements = 1000000

	// ProbLocalCollide 本地交易池中BloomFilter的碰撞概率
	ProbLocalCollide = 0.00001

	// MaxTxPackNum 单个交易池打包交易时的最大账户数量
	MaxTxPackNum = 128

	//POS时隙（s）
	PosTimeSlot = 17
)

// var 其它配置
var (

	// MaxAccountNonce 交易中的nonce最大值（一旦nonce达到这个值，从0继续，同时也是生成账户时初始的最大随机数
	MaxAccountNonce = big.NewInt(1000000)

	// NodeCodeName P2P节点的代号，用于数据库命名、local.bf.gz命名(在main中启动协程batch执行前根据节点私钥被赋值)
	NodeCodeName string
)
