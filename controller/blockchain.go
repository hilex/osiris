package controller

import (
	"github.com/gin-gonic/gin"
	//"net/http"
	//"osiris/dao"
	//"osiris/dto"
)

/*
推荐的传递数据/参数的方式：
GET 路径
POST 表单  x-www-form-urlencoded
POST DTO  json
*/

// BlockchainController 区块相关的业务逻辑
type BlockchainController struct{}

func (controller BlockchainController) GetHandler(context *gin.Context) {
	//TODO
}

/* // AppendHandler 往区块链中添加区块
//
//	@receiver controller
//	@param context
func (controller BlockchainController) AppendHandler(context *gin.Context) {
	var blockDto dto.BlockDto

	if err := context.BindJSON(&blockDto); err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	if err := dao.AddBlock(blockDto); err != nil {
		context.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	context.JSON(http.StatusOK, gin.H{"message": "block append success!"})
} */
