# Details

Date : 2023-11-27 23:02:52

Directory e:\\VSCodeProject\\Osiris

Total : 118 files,  11258 codes, 1700 comments, 1848 blanks, all 14806 lines

[Summary](results.md) / Details / [Diff Summary](diff.md) / [Diff Details](diff-details.md)

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [.idea/inspectionProfiles/Project_Default.xml](/.idea/inspectionProfiles/Project_Default.xml) | XML | 12 | 0 | 0 | 12 |
| [.idea/modules.xml](/.idea/modules.xml) | XML | 8 | 0 | 0 | 8 |
| [.idea/osiris.iml](/.idea/osiris.iml) | XML | 9 | 0 | 0 | 9 |
| [.idea/vcs.xml](/.idea/vcs.xml) | XML | 6 | 0 | 0 | 6 |
| [cli/cli.go](/cli/cli.go) | Go | 119 | 8 | 28 | 155 |
| [cli/cli_manager.go](/cli/cli_manager.go) | Go | 178 | 39 | 47 | 264 |
| [config/config.go](/config/config.go) | Go | 18 | 15 | 16 | 49 |
| [controller/account.go](/controller/account.go) | Go | 125 | 23 | 26 | 174 |
| [controller/blockchain.go](/controller/blockchain.go) | Go | 7 | 30 | 6 | 43 |
| [controller/peer.go](/controller/peer.go) | Go | 49 | 0 | 9 | 58 |
| [controller/test.go](/controller/test.go) | Go | 38 | 6 | 9 | 53 |
| [core/consensus/consensus_manager.go](/core/consensus/consensus_manager.go) | Go | 55 | 11 | 18 | 84 |
| [core/consensus/pos.go](/core/consensus/pos.go) | Go | 272 | 53 | 54 | 379 |
| [core/consensus/trie_tx.go](/core/consensus/trie_tx.go) | Go | 33 | 20 | 10 | 63 |
| [core/conversion/block.go](/core/conversion/block.go) | Go | 63 | 1 | 8 | 72 |
| [core/conversion/peer.go](/core/conversion/peer.go) | Go | 54 | 24 | 14 | 92 |
| [core/conversion/tx.go](/core/conversion/tx.go) | Go | 17 | 0 | 3 | 20 |
| [core/routine/routine_async.go](/core/routine/routine_async.go) | Go | 14 | 4 | 6 | 24 |
| [core/routine/routine_atomic.go](/core/routine/routine_atomic.go) | Go | 104 | 15 | 35 | 154 |
| [core/routine/routine_ordered.go](/core/routine/routine_ordered.go) | Go | 37 | 16 | 19 | 72 |
| [core/state/state_manager.go](/core/state/state_manager.go) | Go | 314 | 77 | 74 | 465 |
| [core/state/trie.go](/core/state/trie.go) | Go | 63 | 34 | 24 | 121 |
| [core/state/trie_account.go](/core/state/trie_account.go) | Go | 108 | 27 | 26 | 161 |
| [core/state/trie_peer.go](/core/state/trie_peer.go) | Go | 132 | 7 | 28 | 167 |
| [core/state/txhandler.go](/core/state/txhandler.go) | Go | 103 | 18 | 30 | 151 |
| [core/state/txhandler_deposit.go](/core/state/txhandler_deposit.go) | Go | 89 | 7 | 15 | 111 |
| [core/state/txhandler_pledge.go](/core/state/txhandler_pledge.go) | Go | 150 | 14 | 19 | 183 |
| [core/state/txhandler_register.go](/core/state/txhandler_register.go) | Go | 58 | 5 | 11 | 74 |
| [core/state/txhandler_reward.go](/core/state/txhandler_reward.go) | Go | 74 | 4 | 14 | 92 |
| [core/state/txhandler_transform.go](/core/state/txhandler_transform.go) | Go | 119 | 13 | 19 | 151 |
| [core/state/txhanlder_release.go](/core/state/txhanlder_release.go) | Go | 143 | 11 | 17 | 171 |
| [core/sys.go](/core/sys.go) | Go | 13 | 5 | 4 | 22 |
| [core/txpool/txpool.go](/core/txpool/txpool.go) | Go | 81 | 45 | 25 | 151 |
| [core/txpool/txpool_base.go](/core/txpool/txpool_base.go) | Go | 219 | 36 | 58 | 313 |
| [core/txpool/txpool_local.go](/core/txpool/txpool_local.go) | Go | 173 | 48 | 46 | 267 |
| [core/txpool/txpool_manager.go](/core/txpool/txpool_manager.go) | Go | 147 | 16 | 44 | 207 |
| [dao/account.go](/dao/account.go) | Go | 71 | 31 | 15 | 117 |
| [dao/addr.go](/dao/addr.go) | Go | 55 | 26 | 13 | 94 |
| [dao/block.go](/dao/block.go) | Go | 101 | 29 | 22 | 152 |
| [dao/local.go](/dao/local.go) | Go | 65 | 9 | 16 | 90 |
| [dao/pubkey.go](/dao/pubkey.go) | Go | 34 | 17 | 9 | 60 |
| [dao/tx.go](/dao/tx.go) | Go | 45 | 0 | 10 | 55 |
| [db/mysql.go](/db/mysql.go) | Go | 83 | 17 | 20 | 120 |
| [db/redis.go](/db/redis.go) | Go | 1 | 0 | 2 | 3 |
| [developer.log](/developer.log) | Log | 32 | 0 | 3 | 35 |
| [dto/account.go](/dto/account.go) | Go | 40 | 32 | 21 | 93 |
| [dto/base.go](/dto/base.go) | Go | 51 | 26 | 15 | 92 |
| [dto/block.go](/dto/block.go) | Go | 80 | 21 | 30 | 131 |
| [dto/peer.go](/dto/peer.go) | Go | 79 | 53 | 31 | 163 |
| [dto/tx.go](/dto/tx.go) | Go | 204 | 70 | 62 | 336 |
| [errors/account.go](/errors/account.go) | Go | 15 | 1 | 7 | 23 |
| [errors/block.go](/errors/block.go) | Go | 7 | 0 | 3 | 10 |
| [errors/peer.go](/errors/peer.go) | Go | 17 | 0 | 7 | 24 |
| [errors/state.go](/errors/state.go) | Go | 29 | 0 | 15 | 44 |
| [errors/std.go](/errors/std.go) | Go | 7 | 0 | 3 | 10 |
| [go.mod](/go.mod) | Go Module File | 185 | 0 | 4 | 189 |
| [go.sum](/go.sum) | Go Checksum File | 1,716 | 0 | 1 | 1,717 |
| [logger/logger.go](/logger/logger.go) | Go | 172 | 7 | 33 | 212 |
| [main.go](/main.go) | Go | 49 | 10 | 12 | 71 |
| [model/account.go](/model/account.go) | Go | 12 | 11 | 9 | 32 |
| [model/addr.go](/model/addr.go) | Go | 9 | 10 | 7 | 26 |
| [model/block.go](/model/block.go) | Go | 18 | 16 | 15 | 49 |
| [model/local.go](/model/local.go) | Go | 28 | 21 | 22 | 71 |
| [model/pubkey.go](/model/pubkey.go) | Go | 9 | 9 | 6 | 24 |
| [model/tx.go](/model/tx.go) | Go | 14 | 9 | 11 | 34 |
| [mpt/branch.go](/mpt/branch.go) | Go | 51 | 6 | 13 | 70 |
| [mpt/crypto.go](/mpt/crypto.go) | Go | 9 | 1 | 3 | 13 |
| [mpt/empty.go](/mpt/empty.go) | Go | 9 | 0 | 4 | 13 |
| [mpt/erc20_proof.go](/mpt/erc20_proof.go) | Go | 23 | 0 | 7 | 30 |
| [mpt/extension.go](/mpt/extension.go) | Go | 30 | 0 | 7 | 37 |
| [mpt/leaf.go](/mpt/leaf.go) | Go | 39 | 0 | 12 | 51 |
| [mpt/nibbles.go](/mpt/nibbles.go) | Go | 77 | 14 | 18 | 109 |
| [mpt/nodes.go](/mpt/nodes.go) | Go | 27 | 0 | 8 | 35 |
| [mpt/proof.go](/mpt/proof.go) | Go | 93 | 21 | 25 | 139 |
| [mpt/storage_proof.go](/mpt/storage_proof.go) | Go | 38 | 0 | 9 | 47 |
| [mpt/trie.go](/mpt/trie.go) | Go | 138 | 43 | 31 | 212 |
| [ocmd/autotx.go](/ocmd/autotx.go) | Go | 37 | 1 | 11 | 49 |
| [ocmd/bootstrap.go](/ocmd/bootstrap.go) | Go | 33 | 1 | 9 | 43 |
| [ocmd/peers.go](/ocmd/peers.go) | Go | 71 | 13 | 13 | 97 |
| [ocmd/ping.go](/ocmd/ping.go) | Go | 65 | 16 | 14 | 95 |
| [ocmd/pos.go](/ocmd/pos.go) | Go | 40 | 5 | 11 | 56 |
| [ocmd/rm.go](/ocmd/rm.go) | Go | 68 | 6 | 20 | 94 |
| [ocmd/std.go](/ocmd/std.go) | Go | 123 | 35 | 33 | 191 |
| [ocmd/txpool.go](/ocmd/txpool.go) | Go | 84 | 1 | 22 | 107 |
| [ocrypto/ecdsa/key.go](/ocrypto/ecdsa/key.go) | Go | 57 | 38 | 16 | 111 |
| [ocrypto/ecdsa/signature.go](/ocrypto/ecdsa/signature.go) | Go | 41 | 41 | 13 | 95 |
| [ocrypto/ed25519/key.go](/ocrypto/ed25519/key.go) | Go | 186 | 43 | 45 | 274 |
| [ocrypto/ed25519/signature.go](/ocrypto/ed25519/signature.go) | Go | 31 | 12 | 9 | 52 |
| [ocrypto/rand/nonce.go](/ocrypto/rand/nonce.go) | Go | 14 | 5 | 4 | 23 |
| [p2p/bootstrap.go](/p2p/bootstrap.go) | Go | 130 | 17 | 27 | 174 |
| [p2p/discovery.go](/p2p/discovery.go) | Go | 88 | 12 | 12 | 112 |
| [p2p/host.go](/p2p/host.go) | Go | 243 | 62 | 55 | 360 |
| [p2p/multicast.go](/p2p/multicast.go) | Go | 48 | 14 | 13 | 75 |
| [p2p/opubsub/pubsub.go](/p2p/opubsub/pubsub.go) | Go | 9 | 25 | 6 | 40 |
| [p2p/opubsub/topic.go](/p2p/opubsub/topic.go) | Go | 1 | 68 | 1 | 70 |
| [p2p/opubsub/tx_topic.go](/p2p/opubsub/tx_topic.go) | Go | 1 | 16 | 1 | 18 |
| [p2p/ping.go](/p2p/ping.go) | Go | 137 | 20 | 28 | 185 |
| [p2p/pos.go](/p2p/pos.go) | Go | 101 | 19 | 26 | 146 |
| [p2p/protocol.go](/p2p/protocol.go) | Go | 12 | 9 | 7 | 28 |
| [p2p/tx.go](/p2p/tx.go) | Go | 86 | 10 | 22 | 118 |
| [proc/start.go](/proc/start.go) | Go | 169 | 41 | 32 | 242 |
| [proc/terminate.go](/proc/terminate.go) | Go | 58 | 16 | 10 | 84 |
| [readme.md](/readme.md) | Markdown | 52 | 0 | 38 | 90 |
| [router/router.go](/router/router.go) | Go | 48 | 12 | 13 | 73 |
| [runtime/log/fuvgingxsgimgcuzxt/debug_2023-11-27.log](/runtime/log/fuvgingxsgimgcuzxt/debug_2023-11-27.log) | Log | 14 | 0 | 1 | 15 |
| [runtime/log/fuvgingxsgimgcuzxt/info_2023-11-27.log](/runtime/log/fuvgingxsgimgcuzxt/info_2023-11-27.log) | Log | 555 | 0 | 1 | 556 |
| [runtime/log/fuvgingxsgimgcuzxt/success_2023-11-27.log](/runtime/log/fuvgingxsgimgcuzxt/success_2023-11-27.log) | Log | 0 | 0 | 1 | 1 |
| [runtime/log/fuvgingxsgimgcuzxt/warn_2023-11-27.log](/runtime/log/fuvgingxsgimgcuzxt/warn_2023-11-27.log) | Log | 4 | 0 | 1 | 5 |
| [runtime/log/info_2023-11-27.log](/runtime/log/info_2023-11-27.log) | Log | 4 | 0 | 1 | 5 |
| [runtime/log/nclcuifoxhfyjjqyzpin/debug_2023-11-27.log](/runtime/log/nclcuifoxhfyjjqyzpin/debug_2023-11-27.log) | Log | 14 | 0 | 1 | 15 |
| [runtime/log/nclcuifoxhfyjjqyzpin/info_2023-11-27.log](/runtime/log/nclcuifoxhfyjjqyzpin/info_2023-11-27.log) | Log | 533 | 0 | 1 | 534 |
| [runtime/log/nclcuifoxhfyjjqyzpin/success_2023-11-27.log](/runtime/log/nclcuifoxhfyjjqyzpin/success_2023-11-27.log) | Log | 0 | 0 | 1 | 1 |
| [runtime/log/uginnrxnhiuxtlxhqwsjtog/debug_2023-11-27.log](/runtime/log/uginnrxnhiuxtlxhqwsjtog/debug_2023-11-27.log) | Log | 10 | 0 | 1 | 11 |
| [runtime/log/uginnrxnhiuxtlxhqwsjtog/info_2023-11-27.log](/runtime/log/uginnrxnhiuxtlxhqwsjtog/info_2023-11-27.log) | Log | 557 | 0 | 1 | 558 |
| [runtime/log/uginnrxnhiuxtlxhqwsjtog/success_2023-11-27.log](/runtime/log/uginnrxnhiuxtlxhqwsjtog/success_2023-11-27.log) | Log | 0 | 0 | 1 | 1 |
| [runtime/log/verlygxldvptnziartfbo/debug_2023-11-27.log](/runtime/log/verlygxldvptnziartfbo/debug_2023-11-27.log) | Log | 12 | 0 | 1 | 13 |
| [runtime/log/verlygxldvptnziartfbo/info_2023-11-27.log](/runtime/log/verlygxldvptnziartfbo/info_2023-11-27.log) | Log | 556 | 0 | 1 | 557 |
| [runtime/log/verlygxldvptnziartfbo/success_2023-11-27.log](/runtime/log/verlygxldvptnziartfbo/success_2023-11-27.log) | Log | 0 | 0 | 1 | 1 |

[Summary](results.md) / Details / [Diff Summary](diff.md) / [Diff Details](diff-details.md)