# Diff Details

Date : 2023-11-27 23:02:52

Directory e:\\VSCodeProject\\Osiris

Total : 87 files,  2281 codes, 263 comments, 327 blanks, all 2871 lines

[Summary](results.md) / [Details](details.md) / [Diff Summary](diff.md) / Diff Details

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [cli/cli.go](/cli/cli.go) | Go | 51 | 4 | 11 | 66 |
| [cli/cli_manager.go](/cli/cli_manager.go) | Go | 35 | 13 | 11 | 59 |
| [config/config.go](/config/config.go) | Go | 2 | 2 | 2 | 6 |
| [controller/account.go](/controller/account.go) | Go | -8 | -5 | -5 | -18 |
| [controller/blockchain.go](/controller/blockchain.go) | Go | -15 | 18 | -3 | 0 |
| [controller/peer.go](/controller/peer.go) | Go | -1 | 0 | 0 | -1 |
| [controller/test.go](/controller/test.go) | Go | -17 | -4 | -3 | -24 |
| [core/block.go](/core/block.go) | Go | -88 | -9 | -12 | -109 |
| [core/blockchain.go](/core/blockchain.go) | Go | -27 | -10 | -6 | -43 |
| [core/consensus/consensus_manager.go](/core/consensus/consensus_manager.go) | Go | 55 | 11 | 18 | 84 |
| [core/consensus/pos.go](/core/consensus/pos.go) | Go | 272 | 53 | 54 | 379 |
| [core/consensus/trie_tx.go](/core/consensus/trie_tx.go) | Go | 33 | 20 | 10 | 63 |
| [core/conversion/block.go](/core/conversion/block.go) | Go | 63 | 1 | 8 | 72 |
| [core/conversion/peer.go](/core/conversion/peer.go) | Go | 54 | 24 | 14 | 92 |
| [core/conversion/tx.go](/core/conversion/tx.go) | Go | 17 | 0 | 3 | 20 |
| [core/peer.go](/core/peer.go) | Go | -49 | -24 | -13 | -86 |
| [core/state/state_manager.go](/core/state/state_manager.go) | Go | 170 | 43 | 37 | 250 |
| [core/state/trie.go](/core/state/trie.go) | Go | 12 | 2 | 4 | 18 |
| [core/state/trie_account.go](/core/state/trie_account.go) | Go | 0 | 1 | 0 | 1 |
| [core/state/trie_peer.go](/core/state/trie_peer.go) | Go | 98 | 4 | 18 | 120 |
| [core/state/txhandler.go](/core/state/txhandler.go) | Go | 0 | 1 | 1 | 2 |
| [core/state/txhandler_deposit.go](/core/state/txhandler_deposit.go) | Go | -4 | 0 | -1 | -5 |
| [core/state/txhandler_pledge.go](/core/state/txhandler_pledge.go) | Go | -11 | 1 | -2 | -12 |
| [core/state/txhandler_register.go](/core/state/txhandler_register.go) | Go | -1 | 0 | 0 | -1 |
| [core/state/txhandler_reward.go](/core/state/txhandler_reward.go) | Go | 74 | 4 | 14 | 92 |
| [core/state/txhandler_transform.go](/core/state/txhandler_transform.go) | Go | -5 | 0 | -2 | -7 |
| [core/state/txhanlder_release.go](/core/state/txhanlder_release.go) | Go | -12 | 0 | -2 | -14 |
| [core/txpool/txpool.go](/core/txpool/txpool.go) | Go | -4 | 2 | -1 | -3 |
| [core/txpool/txpool_base.go](/core/txpool/txpool_base.go) | Go | 78 | 2 | 22 | 102 |
| [core/txpool/txpool_local.go](/core/txpool/txpool_local.go) | Go | 24 | 3 | 8 | 35 |
| [core/txpool/txpool_manager.go](/core/txpool/txpool_manager.go) | Go | 82 | 9 | 24 | 115 |
| [dao/block.go](/dao/block.go) | Go | 101 | 29 | 22 | 152 |
| [dao/blockchain.go](/dao/blockchain.go) | Go | -93 | -28 | -16 | -137 |
| [dao/tx.go](/dao/tx.go) | Go | 45 | 0 | 10 | 55 |
| [db/mysql.go](/db/mysql.go) | Go | 5 | 0 | 1 | 6 |
| [developer.log](/developer.log) | Log | 1 | 0 | 0 | 1 |
| [dto/account.go](/dto/account.go) | Go | 1 | 1 | 1 | 3 |
| [dto/block.go](/dto/block.go) | Go | 65 | 21 | 27 | 113 |
| [dto/tx.go](/dto/tx.go) | Go | 11 | 1 | 3 | 15 |
| [errors/peer.go](/errors/peer.go) | Go | 6 | 0 | 0 | 6 |
| [errors/state.go](/errors/state.go) | Go | 4 | 0 | 2 | 6 |
| [main.go](/main.go) | Go | 1 | 0 | 0 | 1 |
| [model/block.go](/model/block.go) | Go | 18 | 16 | 15 | 49 |
| [model/blockchain.go](/model/blockchain.go) | Go | -13 | -4 | -3 | -20 |
| [model/tx.go](/model/tx.go) | Go | 14 | 9 | 11 | 34 |
| [mpt/nodes.go](/mpt/nodes.go) | Go | 2 | 0 | 0 | 2 |
| [mpt/proof.go](/mpt/proof.go) | Go | 0 | 11 | 0 | 11 |
| [mpt/trie.go](/mpt/trie.go) | Go | 0 | 4 | 0 | 4 |
| [ocmd/pos.go](/ocmd/pos.go) | Go | 40 | 5 | 11 | 56 |
| [ocmd/rm.go](/ocmd/rm.go) | Go | 8 | 0 | 2 | 10 |
| [ocmd/std.go](/ocmd/std.go) | Go | 4 | 1 | 2 | 7 |
| [ocrypto/ecdsa/signature.go](/ocrypto/ecdsa/signature.go) | Go | 4 | 3 | 1 | 8 |
| [p2p/host.go](/p2p/host.go) | Go | 22 | 6 | 3 | 31 |
| [p2p/multicast.go](/p2p/multicast.go) | Go | 3 | 2 | 2 | 7 |
| [p2p/pos.go](/p2p/pos.go) | Go | 101 | 19 | 26 | 146 |
| [p2p/tx.go](/p2p/tx.go) | Go | 1 | 0 | -1 | 0 |
| [proc/start.go](/proc/start.go) | Go | 3 | 1 | 1 | 5 |
| [router/router.go](/router/router.go) | Go | -2 | 0 | 0 | -2 |
| [runtime/log/fuvgingxsgimgcuzxt/debug_2023-11-18.log](/runtime/log/fuvgingxsgimgcuzxt/debug_2023-11-18.log) | Log | -15 | 0 | -1 | -16 |
| [runtime/log/fuvgingxsgimgcuzxt/debug_2023-11-27.log](/runtime/log/fuvgingxsgimgcuzxt/debug_2023-11-27.log) | Log | 14 | 0 | 1 | 15 |
| [runtime/log/fuvgingxsgimgcuzxt/error_2023-11-18.log](/runtime/log/fuvgingxsgimgcuzxt/error_2023-11-18.log) | Log | -43 | 0 | -2 | -45 |
| [runtime/log/fuvgingxsgimgcuzxt/info_2023-11-18.log](/runtime/log/fuvgingxsgimgcuzxt/info_2023-11-18.log) | Log | -305 | 0 | -1 | -306 |
| [runtime/log/fuvgingxsgimgcuzxt/info_2023-11-27.log](/runtime/log/fuvgingxsgimgcuzxt/info_2023-11-27.log) | Log | 555 | 0 | 1 | 556 |
| [runtime/log/fuvgingxsgimgcuzxt/success_2023-11-18.log](/runtime/log/fuvgingxsgimgcuzxt/success_2023-11-18.log) | Log | -12 | 0 | -1 | -13 |
| [runtime/log/fuvgingxsgimgcuzxt/success_2023-11-27.log](/runtime/log/fuvgingxsgimgcuzxt/success_2023-11-27.log) | Log | 0 | 0 | 1 | 1 |
| [runtime/log/fuvgingxsgimgcuzxt/warn_2023-11-18.log](/runtime/log/fuvgingxsgimgcuzxt/warn_2023-11-18.log) | Log | -5 | 0 | -1 | -6 |
| [runtime/log/fuvgingxsgimgcuzxt/warn_2023-11-27.log](/runtime/log/fuvgingxsgimgcuzxt/warn_2023-11-27.log) | Log | 4 | 0 | 1 | 5 |
| [runtime/log/info_2023-11-18.log](/runtime/log/info_2023-11-18.log) | Log | -10 | 0 | -1 | -11 |
| [runtime/log/info_2023-11-27.log](/runtime/log/info_2023-11-27.log) | Log | 4 | 0 | 1 | 5 |
| [runtime/log/nclcuifoxhfyjjqyzpin/debug_2023-11-18.log](/runtime/log/nclcuifoxhfyjjqyzpin/debug_2023-11-18.log) | Log | -16 | 0 | -1 | -17 |
| [runtime/log/nclcuifoxhfyjjqyzpin/debug_2023-11-27.log](/runtime/log/nclcuifoxhfyjjqyzpin/debug_2023-11-27.log) | Log | 14 | 0 | 1 | 15 |
| [runtime/log/nclcuifoxhfyjjqyzpin/info_2023-11-18.log](/runtime/log/nclcuifoxhfyjjqyzpin/info_2023-11-18.log) | Log | -258 | 0 | -1 | -259 |
| [runtime/log/nclcuifoxhfyjjqyzpin/info_2023-11-27.log](/runtime/log/nclcuifoxhfyjjqyzpin/info_2023-11-27.log) | Log | 533 | 0 | 1 | 534 |
| [runtime/log/nclcuifoxhfyjjqyzpin/success_2023-11-18.log](/runtime/log/nclcuifoxhfyjjqyzpin/success_2023-11-18.log) | Log | 0 | 0 | -1 | -1 |
| [runtime/log/nclcuifoxhfyjjqyzpin/success_2023-11-27.log](/runtime/log/nclcuifoxhfyjjqyzpin/success_2023-11-27.log) | Log | 0 | 0 | 1 | 1 |
| [runtime/log/uginnrxnhiuxtlxhqwsjtog/debug_2023-11-18.log](/runtime/log/uginnrxnhiuxtlxhqwsjtog/debug_2023-11-18.log) | Log | -14 | 0 | -1 | -15 |
| [runtime/log/uginnrxnhiuxtlxhqwsjtog/debug_2023-11-27.log](/runtime/log/uginnrxnhiuxtlxhqwsjtog/debug_2023-11-27.log) | Log | 10 | 0 | 1 | 11 |
| [runtime/log/uginnrxnhiuxtlxhqwsjtog/info_2023-11-18.log](/runtime/log/uginnrxnhiuxtlxhqwsjtog/info_2023-11-18.log) | Log | -252 | 0 | -1 | -253 |
| [runtime/log/uginnrxnhiuxtlxhqwsjtog/info_2023-11-27.log](/runtime/log/uginnrxnhiuxtlxhqwsjtog/info_2023-11-27.log) | Log | 557 | 0 | 1 | 558 |
| [runtime/log/uginnrxnhiuxtlxhqwsjtog/success_2023-11-18.log](/runtime/log/uginnrxnhiuxtlxhqwsjtog/success_2023-11-18.log) | Log | 0 | 0 | -1 | -1 |
| [runtime/log/uginnrxnhiuxtlxhqwsjtog/success_2023-11-27.log](/runtime/log/uginnrxnhiuxtlxhqwsjtog/success_2023-11-27.log) | Log | 0 | 0 | 1 | 1 |
| [runtime/log/verlygxldvptnziartfbo/debug_2023-11-18.log](/runtime/log/verlygxldvptnziartfbo/debug_2023-11-18.log) | Log | -16 | 0 | -1 | -17 |
| [runtime/log/verlygxldvptnziartfbo/debug_2023-11-27.log](/runtime/log/verlygxldvptnziartfbo/debug_2023-11-27.log) | Log | 12 | 0 | 1 | 13 |
| [runtime/log/verlygxldvptnziartfbo/info_2023-11-18.log](/runtime/log/verlygxldvptnziartfbo/info_2023-11-18.log) | Log | -262 | 0 | -1 | -263 |
| [runtime/log/verlygxldvptnziartfbo/info_2023-11-27.log](/runtime/log/verlygxldvptnziartfbo/info_2023-11-27.log) | Log | 556 | 0 | 1 | 557 |
| [runtime/log/verlygxldvptnziartfbo/success_2023-11-18.log](/runtime/log/verlygxldvptnziartfbo/success_2023-11-18.log) | Log | 0 | 0 | -1 | -1 |
| [runtime/log/verlygxldvptnziartfbo/success_2023-11-27.log](/runtime/log/verlygxldvptnziartfbo/success_2023-11-27.log) | Log | 0 | 0 | 1 | 1 |

[Summary](results.md) / [Details](details.md) / [Diff Summary](diff.md) / Diff Details