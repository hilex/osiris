# Diff Summary

Date : 2023-11-27 23:02:52

Directory e:\\VSCodeProject\\Osiris

Total : 87 files,  2281 codes, 263 comments, 327 blanks, all 2871 lines

[Summary](results.md) / [Details](details.md) / Diff Summary / [Diff Details](diff-details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| Go | 57 | 1,229 | 263 | 329 | 1,821 |
| Log | 30 | 1,052 | 0 | -2 | 1,050 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 87 | 2,281 | 263 | 327 | 2,871 |
| . (Files) | 2 | 2 | 0 | 0 | 2 |
| cli | 2 | 86 | 17 | 22 | 125 |
| config | 1 | 2 | 2 | 2 | 6 |
| controller | 4 | -41 | 9 | -11 | -43 |
| core | 24 | 831 | 138 | 196 | 1,165 |
| core (Files) | 3 | -164 | -43 | -31 | -238 |
| core\\consensus | 3 | 360 | 84 | 82 | 526 |
| core\\conversion | 3 | 134 | 25 | 25 | 184 |
| core\\state | 11 | 321 | 56 | 67 | 444 |
| core\\txpool | 4 | 180 | 16 | 53 | 249 |
| dao | 3 | 53 | 1 | 16 | 70 |
| db | 1 | 5 | 0 | 1 | 6 |
| dto | 3 | 77 | 23 | 31 | 131 |
| errors | 2 | 10 | 0 | 2 | 12 |
| model | 3 | 19 | 21 | 23 | 63 |
| mpt | 3 | 2 | 15 | 0 | 17 |
| ocmd | 3 | 52 | 6 | 15 | 73 |
| ocrypto | 1 | 4 | 3 | 1 | 8 |
| ocrypto\\ecdsa | 1 | 4 | 3 | 1 | 8 |
| p2p | 4 | 127 | 27 | 30 | 184 |
| proc | 1 | 3 | 1 | 1 | 5 |
| router | 1 | -2 | 0 | 0 | -2 |
| runtime | 29 | 1,051 | 0 | -2 | 1,049 |
| runtime\\log | 29 | 1,051 | 0 | -2 | 1,049 |
| runtime\\log (Files) | 2 | -6 | 0 | 0 | -6 |
| runtime\\log\\fuvgingxsgimgcuzxt | 9 | 193 | 0 | -2 | 191 |
| runtime\\log\\nclcuifoxhfyjjqyzpin | 6 | 273 | 0 | 0 | 273 |
| runtime\\log\\uginnrxnhiuxtlxhqwsjtog | 6 | 301 | 0 | 0 | 301 |
| runtime\\log\\verlygxldvptnziartfbo | 6 | 290 | 0 | 0 | 290 |

[Summary](results.md) / [Details](details.md) / Diff Summary / [Diff Details](diff-details.md)