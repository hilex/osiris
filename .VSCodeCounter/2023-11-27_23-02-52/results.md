# Summary

Date : 2023-11-27 23:02:52

Directory e:\\VSCodeProject\\Osiris

Total : 118 files,  11258 codes, 1700 comments, 1848 blanks, all 14806 lines

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| Go | 96 | 6,979 | 1,700 | 1,788 | 10,467 |
| Log | 15 | 2,291 | 0 | 17 | 2,308 |
| Go Checksum File | 1 | 1,716 | 0 | 1 | 1,717 |
| Go Module File | 1 | 185 | 0 | 4 | 189 |
| Markdown | 1 | 52 | 0 | 38 | 90 |
| XML | 4 | 35 | 0 | 0 | 35 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 118 | 11,258 | 1,700 | 1,848 | 14,806 |
| . (Files) | 5 | 2,034 | 10 | 58 | 2,102 |
| .idea | 4 | 35 | 0 | 0 | 35 |
| .idea (Files) | 3 | 23 | 0 | 0 | 23 |
| .idea\\inspectionProfiles | 1 | 12 | 0 | 0 | 12 |
| cli | 2 | 297 | 47 | 75 | 419 |
| config | 1 | 18 | 15 | 16 | 49 |
| controller | 4 | 219 | 59 | 50 | 328 |
| core | 25 | 2,635 | 511 | 621 | 3,767 |
| core (Files) | 1 | 13 | 5 | 4 | 22 |
| core\\consensus | 3 | 360 | 84 | 82 | 526 |
| core\\conversion | 3 | 134 | 25 | 25 | 184 |
| core\\routine | 3 | 155 | 35 | 60 | 250 |
| core\\state | 11 | 1,353 | 217 | 277 | 1,847 |
| core\\txpool | 4 | 620 | 145 | 173 | 938 |
| dao | 6 | 371 | 112 | 85 | 568 |
| db | 2 | 84 | 17 | 22 | 123 |
| dto | 5 | 454 | 202 | 159 | 815 |
| errors | 5 | 75 | 1 | 35 | 111 |
| logger | 1 | 172 | 7 | 33 | 212 |
| model | 6 | 90 | 76 | 70 | 236 |
| mpt | 11 | 534 | 85 | 137 | 756 |
| ocmd | 8 | 521 | 78 | 133 | 732 |
| ocrypto | 5 | 329 | 139 | 87 | 555 |
| ocrypto\\ecdsa | 2 | 98 | 79 | 29 | 206 |
| ocrypto\\ed25519 | 2 | 217 | 55 | 54 | 326 |
| ocrypto\\rand | 1 | 14 | 5 | 4 | 23 |
| p2p | 11 | 856 | 272 | 198 | 1,326 |
| p2p (Files) | 8 | 845 | 163 | 190 | 1,198 |
| p2p\\opubsub | 3 | 11 | 109 | 8 | 128 |
| proc | 2 | 227 | 57 | 42 | 326 |
| router | 1 | 48 | 12 | 13 | 73 |
| runtime | 14 | 2,259 | 0 | 14 | 2,273 |
| runtime\\log | 14 | 2,259 | 0 | 14 | 2,273 |
| runtime\\log (Files) | 1 | 4 | 0 | 1 | 5 |
| runtime\\log\\fuvgingxsgimgcuzxt | 4 | 573 | 0 | 4 | 577 |
| runtime\\log\\nclcuifoxhfyjjqyzpin | 3 | 547 | 0 | 3 | 550 |
| runtime\\log\\uginnrxnhiuxtlxhqwsjtog | 3 | 567 | 0 | 3 | 570 |
| runtime\\log\\verlygxldvptnziartfbo | 3 | 568 | 0 | 3 | 571 |

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)